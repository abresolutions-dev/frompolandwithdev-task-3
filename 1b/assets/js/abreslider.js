/*
AbreSlider - FullWidth Image Slider for Frompolandwithdev.com from AbreSolutions
Create date: 06.2018
Main Todos:
- code reduction to remove redundant code 
- get current slide number after each change
- set current slide class 'isActive'
- set active pagination class
- set default changeSpeed and auto values
- add touch gesture for mobile
*/
$.fn.abreSlider = function(changeSpeed, auto) {
    var sliderItems = $('.abreSlider .slide');
    var leftArrow = '<img class="arrow left" src="assets/images/abreslider/leftArrow.png">';
    var rightArrow = '<img class="arrow right" src="assets/images/abreslider/rightArrow.png">';
    var abreSliderWidth = 0;
    var maxAbreSliderWidth = 0;
    var isActiveWidth = 0;
    var currSliderWidth = 0;
    var currTransition = 0;
    var toTransition = 0;
    var maxTransition = 0;
    var pagination = '';
    var goToslideNr = 0;    
    if( sliderItems.length > 0 ){
        $('.abreSlider').wrap('<div class="abreSliderStage"></div>');
    	abreSliderWidth = $('.abreSliderStage').width();    	
    	$('.abreSlider .slide').css('max-width', abreSliderWidth);
        $('.abreSlider').attr('currTransition', toTransition);
        isActiveWidth = $('.abreSlider .slide:first').outerWidth();
        
        //Here we use the first changeSpeed - it is in ms so we need to divide it by 100 to use CSS style - transition - in s 
        $('.abreSlider').css('transition', changeSpeed/1000+'s');
        
        pagination = '<div class="pagination">';
        $('.abreSlider .slide').each(function( index ) {            
            maxAbreSliderWidth += $(this).width();
            slideNr = index+1;
            pagination += '<span class="goToSlide" slideNr='+index+'>'+slideNr+'</span>';
        });
        pagination += '</div>';
        maxTransition = parseInt(maxAbreSliderWidth-isActiveWidth);
        $('.abreSlider').attr('maxTransition', maxTransition);
        $('.abreSlider').width(maxAbreSliderWidth);
        $('.abreSlider .slide').addClass('isAbreSlide');
        $('.abreSlider .slide:first').addClass('isActive');
        $('.abreSliderStage').prepend(leftArrow);
        $('.abreSliderStage').append(rightArrow);
        $('.abreSliderStage').append(pagination);
    }
    goTransition = function(transition){
        $('.abreSlider').attr('currTransition', transition);
        $('.abreSlider').css('transform', 'translate3d('+transition+'px, 0px, 0px)');
    }
    changeImage = function(){
	    minusMaxTransition = 0-maxTransition;
	    currTransition = parseInt($('.abreSlider').attr('currTransition'));
    	if( currTransition==minusMaxTransition || currTransition==maxTransition ){
    		goToslideNr = 0;
	        currSliderWidth = parseInt($('.isActive').outerWidth());       
	        toTransition = 0;
	        currTransition = 0;
	        goTransition(toTransition);
    	}else{
	    	currSliderWidth = parseInt($('.isActive').outerWidth());
	        if( currTransition<=0 ){
	        	toTransition = currTransition-currSliderWidth;   
                goTransition(toTransition);
	        }else{
	            toTransition = 0;
                goTransition(toTransition);
	        }
	      }
    }
    
    //Here we use the sceond variable auto to use JS interval function - it get ms so we don't need to convert it 
    var slidesInterval = setInterval(changeImage, auto);
    $('.arrow.left').on('click', function(){
    	clearInterval(slidesInterval);
        currSliderWidth = parseInt($('.isActive').outerWidth());
        currTransition = parseInt($('.abreSlider').attr('currTransition'));      
        if( currTransition==0 ){
        	$('.abreSlider').css('transform', 'translate3d(50px, 0px, 0px)');
        	setTimeout(
        		function(){
        			$('.abreSlider').css('transform', 'translate3d(0px, 0px, 0px)')
        		}, 500);
        }else if( currTransition<0 ){
        	toTransition = currTransition+currSliderWidth;  
            goTransition(toTransition);
        }else{
        	toTransition = currTransition-currSliderWidth;  
            goTransition(-toTransition);     	
        }
        slidesInterval = setInterval(changeImage, auto);
    });
    $('.arrow.right').on('click', function(){
    	clearInterval(slidesInterval);
        currSliderWidth = parseInt($('.isActive').outerWidth());
        currTransition = parseInt($('.abreSlider').attr('currTransition'));
        minusMaxTransition = 0-maxTransition;
        if( currTransition==minusMaxTransition || currTransition==maxTransition ){
        	timeOutTransition = minusMaxTransition-50;
        	console.log(timeOutTransition);
        	$('.abreSlider').css('transform', 'translate3d('+timeOutTransition+'px, 0px, 0px)');
        	setTimeout(
        		function(){
        			$('.abreSlider').css('transform', 'translate3d('+minusMaxTransition+'px, 0px, 0px)')
        		}, 500);
        }else if( currTransition<=0 ){
        	toTransition = currTransition-currSliderWidth;   
            goTransition(toTransition);
        }
        slidesInterval = setInterval(changeImage, auto);
    });
    $('.goToSlide').on('click', function(){
    	clearInterval(slidesInterval);
        goToslideNr = parseInt($(this).attr('slideNr'));
        currSliderWidth = parseInt($('.isActive').outerWidth());       
        toTransition = goToslideNr*currSliderWidth;
        currTransition = 0-goToslideNr*currSliderWidth;
        $('.abreSlider').attr('currTransition', currTransition);
        $('.abreSlider').css('transform', 'translate3d(-'+toTransition+'px, 0px, 0px)');
        slidesInterval = setInterval(changeImage, auto);
    });
}