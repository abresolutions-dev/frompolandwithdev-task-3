function setDiamondTops(){
    var windowWidth = $(window).width();
    if( windowWidth>=576 ){
        var topDiamondHeight = 0;
        var endDiamondHeight = 0;
        $( ".top-diamond" ).each(function( index ) {
            topDiamondHeight = $(this).outerHeight(true);
            if( topDiamondHeight > endDiamondHeight ){
                endDiamondHeight = topDiamondHeight;
            }
        });
        $( ".top-diamond" ).height(endDiamondHeight-4);
    }else{
        $( ".top-diamond" ).height('auto');
    }
}

$(document).ready(function(){
    setDiamondTops();
    $(window).on('resize',function(){
        setDiamondTops();
    });
});